import { Injectable } from '@angular/core';

import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable, throwError, from, config } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  linkAIP = 'http://phimqq.com/@phim/Api';
  linkAIPGame = location.protocol+'//api.egame.ga';
  // linkAIPGame = 'http://api.egame.ga';
  public linkEmbed: string = 'http://localhost/phimqq/Embed';
  constructor(
    private http: HttpClient
  ) {

  }


  search_movie(key_w, limit, offset) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.linkAIP}/Movies/Movie/search/${key_w}/${limit}/${offset}`)
        .subscribe((result) => {
          resolve(result)
        });
    })
  }

  getGames(limit = 5, skip =0) {
     return  this.http.get(`${this.linkAIPGame}/services/?cmd=game&action=get&limit=${limit}&skip=${skip}`)
  }
  
  getDetailGame(title_slug ) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.linkAIPGame}/services/?cmd=game&action=getDetail&title=${title_slug}`).subscribe(res => {
        resolve(res)
      }, err => {
        reject(err)
      })
    });
  }



  getMovies(limit, offset, The_loai) {
    return new Promise((res, err) => {
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'text/plain');
      this.http.post(`${this.linkAIP}/Movies/Movie/${limit}/${offset}/`,{the_loai:The_loai}, {headers})
        .subscribe(data => {
          res(data)
        }, error => err(error))
    })
  }

  // getCategory() {
  //   return new Promise((res, err) => {
  //     this.http.get(`${this.linkAIP}/Movies/The_loai`)
  //       .subscribe(data => {
  //         res(data)
  //       }, error => err(error))

  //   })
  // }
  // getDetail(seo_name) {
  //   return new Promise((resolve, reject) => {
  //     this.http.get(`${this.linkAIP}/Movies/Movie/detail/${seo_name}`).subscribe(res => {
  //       resolve(res)
  //     }, err => {
  //       reject(err)
  //     })
  //   });
  // }

}
