import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { ActionSheetController } from '@ionic/angular';



declare var $;
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public home: string;
  public movies: any;
  public games: any;
  public categories: any;
  public the_loai: any;
  public loadList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  public list_cinema: any;
  public list_cinema_all: any;
  public limit = 30;
  public offset = 0;
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    public api: ApiService,
    public actionSheetController: ActionSheetController
  ) { }


   ngOnInit() {
    this.didLoad();
  }

  async didLoad() {
    this.api.getGames(this.limit,this.offset)
      .subscribe(data => {
         let u:any = data;
         this.games = u.entries;
      }, error => console.log(error))
  }
  async select_the_loai(){
    let movies: any = await this.api.getMovies(this.limit, this.offset,this.the_loai);
    this.movies = movies.data
  }
  
  go(seo_name) {
    this.router.navigateByUrl('/detail/' + seo_name)
  }
  
  open_search() {
    this.router.navigateByUrl('/search');
  }

  // async getMovies() {
  //   let movies: any = await this.api.getMovies(this.limit, this.offset,this.the_loai);
  //   this.movies = movies.data
  
  // }

  //loadData scroll
  async loadData(even) {
    this.limit = this.limit + 30
    this.offset = this.offset + 30
    this.api.getGames(this.limit,this.offset)
      .subscribe(data => {
        let u:any = data;
          console.log(this.games);
          this.games = this.games.concat(u.entries)
          if (this.games.length == 0) {
             even.target.disabled = true;
          }
          even.target.complete();
      }, error => console.log(error))


  }


  ionViewDidEnter() {
    const preloadArea: HTMLElement = document.getElementById('preload');
    preloadArea.appendChild(document.createElement('ion-spinner'));
    preloadArea.appendChild(document.createElement('ion-grid'));
    preloadArea.appendChild(document.createElement('ion-toolbar'));
    preloadArea.appendChild(document.createElement('ion-back-button'));
  }
}
