import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { DomSanitizer} from '@angular/platform-browser';
declare var $;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})

export class DetailPage implements OnInit {
  load = false;
  load_done = true;
  movie: any;
  game: any;
  title_slug: string;
  iframe :any;
  _id: string;
  constructor(
    public router: Router,
    public activeR: ActivatedRoute,
    public api: ApiService,
    private sanitizer: DomSanitizer
  ) { }

  async ngOnInit() {
    await this.activeR.paramMap.subscribe(async data => {
      let data_: any = data
      this.load = true;
      if (data_.params.title_slug != undefined) {
        let game: any = await this.api.getDetailGame(data_.params.title_slug);
        this.game =  game.entries[0];
        this.iframe = this.sanitizer.bypassSecurityTrustHtml(this.game.path_embed)
        console.log(this.game.path_embed);
        // load done
        setTimeout(() => {
          this.load = false;
        }, 1000)

      }
    });
  }

  go_play(title_slug) {
    // this.animation_push();
    setTimeout(() => {
      this.router.navigateByUrl('/play/' + title_slug);
    }, 1000)

  }

  go_info(title_slug) {
    // this.animation_push();
    setTimeout(() => {
      this.router.navigateByUrl('/info/' + title_slug);
    }, 1000)

  }


  // animation_push() {
  //   $('.header').addClass('slide-out-top');
  //   setTimeout(() => {
  //     $('.card-action').addClass('slide-out-bottom');
  //   }, 200)

  //   setTimeout(() => {
  //     $('.title-movie').addClass('tracking-out-contract');
  //     $('.title-movie-sub').addClass('tracking-out-expand');
  //   }, 400)
  //   setTimeout(() => {
  //     $('.box').removeClass('scale-in-ver-center');
  //     $('.box').addClass('slide-out-right');
  //   }, 800)

  //   setTimeout(() => {
  //     $('.header').removeClass('slide-out-top');
  //     $('.card-action').removeClass('slide-out-bottom');
  //     $('.title-movie').removeClass('tracking-out-contract');
  //     $('.title-movie-sub').removeClass('tracking-out-expand');
  //     $('.box').removeClass('slide-out-right');
  //     $('.box').addClass('scale-in-ver-center');
  //   }, 1500)
  // }
}
