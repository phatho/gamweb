import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  public movie: any
  public content
  seo_name
  constructor(
    public api: ApiService,
    public router: Router,
    public activeR: ActivatedRoute,
    public sanitization: DomSanitizer
  ) {

  }

  async ngOnInit() {
    // await this.activeR.paramMap.subscribe(async data => {
    //   let data_: any = data
    //   if (data_.params.seo_name != undefined) {
    //     let movie_: any = await this.api.getDetail(data_.params.seo_name);
    //     this.movie = JSON.parse(movie_.data.data_json);
    //     this.seo_name = movie_.data.seo_name;
    //   }
    // });
  }




  go_play(seo_name) {
    this.router.navigateByUrl('/play/' + seo_name);
  }

}
