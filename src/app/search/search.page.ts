import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { IonInfiniteScroll, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;

  p_list: any;
  key_w: string;
  listType: string;
  page: number = 1;
  title_page: string = 'Phim'
  limit: number = 40;
  offset: number = 0;

  constructor(
    public api: ApiService,
    public load: LoadingController,
    public router: Router,
    
  ) { }

  ngOnInit() { }

  go(seo_name) {
    this.router.navigateByUrl('/detail/' + seo_name)
  }

  async search_movie(Input) {
    this.limit = 18 ;
    this.offset = 0;
    this.key_w = Input.target.value;
    if (this.key_w == "") return;
    let load = await this.load.create();
    load.present();
    let list_search: any = await this.api.search_movie(this.key_w, this.limit, this.offset);
    this.p_list = list_search.data;
    load.dismiss();
  }

  async get_load_ds_movie() {
    let list_search: any = await this.api.search_movie(this.key_w, this.limit, this.offset);
    this.p_list = list_search.data;
  }

  //loadData scroll
  async loadData(even) {
    this.limit = this.limit + 40
    this.offset = this.offset + 40
    var list_search: any = await this.api.search_movie(this.key_w, this.limit, this.offset);
    list_search = list_search.data;
    setTimeout(() => {
      this.p_list = this.p_list.concat(list_search)
      if (list_search.length == 0) {
        even.target.disabled = true;
      }
      even.target.complete();
    }, 300);
  }
}
