import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

declare var $;
@Component({
  selector: 'app-play',
  templateUrl: './play.page.html',
  styleUrls: ['./play.page.scss'],
})

export class PlayPage implements OnInit {
  public iframe;
  public movie;
  public link;
  constructor(
    public api: ApiService,
    public sanitization: DomSanitizer,
    public activeR: ActivatedRoute
  ) { }

  async ngOnInit() {
    // $('#player_video').empty();
    // await this.activeR.paramMap.subscribe(async data => {
    //   let data_: any = data
    //   // console.log(data_.params.seo_name)
    //   if (data_.params.seo_name != undefined) {
    //     let movie_: any = await this.api.getDetail(data_.params.seo_name);
    //     this.movie = JSON.parse(movie_.data.data_json);
    //   }
    //   this.link = this.sanitization.bypassSecurityTrustResourceUrl(this.api.load_iframe(data_.params.seo_name));
    //   this.iframe = this.load_iframe(this.api.load_iframe(data_.params.seo_name));
    // });
  }



  load_iframe(link) {
    return this.sanitization.bypassSecurityTrustHtml(`<iframe  sandbox=" allow-scripts allow-same-origin" class="embed-responsive-item" 
     src=${link} allowfullscreen="true" scrolling="no" width="100%"  style=" border: 0px;"  onload="resizeIframe(this)" ></iframe>`)
  }

}
